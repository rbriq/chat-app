package hw2.agency.server;

public class SuspectedUsers
{
	public static final String ATA_TOTAH_USER = "ata_totah";
	
	public static final String EN_EN_ALEHA_USER = "en_en_aleha";
	
	public static final String META_ALEHA_USER = "meta_aleha";
	
	public static final String ATA_HAGADOL_MIKULAM_USER = "ata_hagadol_mikulam";
}
