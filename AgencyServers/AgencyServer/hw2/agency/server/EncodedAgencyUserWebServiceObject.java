package hw2.agency.server;

//The EncodedAgencyUserWebServiceObject is a wrapper object for an EncodedAgencyUser object
//specifically suitable for the web service communication
public class EncodedAgencyUserWebServiceObject
{
	private String encodedUserName = "";
	
	public EncodedAgencyUserWebServiceObject()
	{
		; //do nothing
	}
	
	public EncodedAgencyUserWebServiceObject(String encodedUserName)
	{
		this.encodedUserName = encodedUserName;
	}
	
	public String getEncodedUserName()
	{
		return encodedUserName;
	}
	
	public void setEncodedUserName(String encodedUserName)
	{
		this.encodedUserName = encodedUserName;
	}
}
