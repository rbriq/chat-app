package hw2.agency.server;

import java.io.Serializable;

//The encoded agency user writes the user name in a reversed order
public class EncodedAgencyUserImpl implements Serializable, EncodedAgencyUser
{
	private static final long serialVersionUID = 1L;
	private String encodedUserName = "";
	
	public EncodedAgencyUserImpl()
	{
		; //do nothing
	}
	
	public EncodedAgencyUserImpl(String userName)
	{
		encodeUserName(userName);
	}

	private void encodeUserName(String userName)
	{
		int i, len = userName.length();
	    StringBuffer dest = new StringBuffer(len);

	    for (i = (len - 1); i >= 0; i--)
	    {
	    	dest.append(userName.charAt(i));
	    }
	    encodedUserName = dest.toString();
	}
	
	private String decodeUserName()
	{
		String decodedUserName = "";
		int i, len = encodedUserName.length();
	    StringBuffer dest = new StringBuffer(len);

	    for (i = (len - 1); i >= 0; i--)
	    {
	    	dest.append(encodedUserName.charAt(i));
	    }
	    decodedUserName = dest.toString();
	    return decodedUserName;
	}
	
	
	public String getEncodedUserName()
	{
		return encodedUserName;
	}
	
	public void setEncodedUserName(String userName)
	{
		encodeUserName(userName);
	}
	
	public String getDecodedUserName()
	{
		return decodeUserName();
	}
	
}
