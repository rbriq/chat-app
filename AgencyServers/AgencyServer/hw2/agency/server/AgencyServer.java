package hw2.agency.server;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.xml.ws.Endpoint;


/**
 * This application publishes the web service whose SIB is
 * hw2.agency.server.AgencyServiceServer. For now, the service is published at network address
 * 127.0.0.1., which is localhost, and at port number 7777, as this port is
 * likely available on any desktop machine. The publication path is /agencywebservice
 * 
 * The Endpoint class has an overloaded publish method. In this two-argument
 * version, the first argument is the publication URL as a string and the second
 * argument is an instance of the service SIB, in this case
 * ackage hw2.agency.server.AgencyServiceServer
 * 
 * The application runs indefinitely, awaiting service requests. It needs to be
 * terminated at the command prompt with control-C or the equivalent.
 * 
 * Once the application is started, open a browser to the URL
 * 
 * http://localhost:7777/agencywebservice?wsdl
 * 
 * to view the service contract, the WSDL document. This is an easy test to
 * determine whether the service has deployed successfully. If the test
 * succeeds, a client then can be executed against the service.
 * 
 * Out of the box, the Endpoint publisher handles one client request at a time. 
 * This is fine for getting web services up and running in development mode. 
 * However, if the processing of a given request should hang, then all other client requests are 
 * effectively blocked, so this implementation is only simplified and in real life we would use 
 * a "container" service to handle multiple requests concurrently.
 */

public class AgencyServer {
	
	public static void main(String[] args)
	{
		// 1st argument is the publication URL
		// 2nd argument is an SIB instance
		Endpoint endpoint = Endpoint.publish("http://localhost:7777/agencywebservice", new AgencyServiceServerImpl());
		
		//Test that it is available
		boolean status = endpoint.isPublished();
		if(status)
		{
			try {
				MessageLog.log("Web service AgencyServiceServer is running on port" +
						" 7777 on host " + InetAddress.getLocalHost().getHostName() + 
						" with address " + InetAddress.getLocalHost().getHostAddress() + "...");
			} catch (UnknownHostException e) {
				System.err.println("error retrieving local host address"
						+ e.getMessage());
				e.printStackTrace();
			}
		}
		else
		{
			System.out.println("Web service AgencyServiceServer failed to start");
		}
	}

}
