package hw2.agency.server;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

/**
 * this class implements the methods declared as web methods and that
 * the agency web service provides.
 * @author NOONA
 *
 */
@WebService(endpointInterface = "hw2.agency.server.AgencyServiceServer") 
public class AgencyServiceServerImpl implements AgencyServiceServer{

	
	public void userJoined(EncodedAgencyUserWebServiceObject encodedAgencyUser) {
		String agencyEncodedUsername = 
			encodedAgencyUser.getEncodedUserName();
	//	EncodedAgencyUser encodedAgencyUser1 = 
		//	new EncodedAgencyUserImpl(agencyEncodedUsername); 
		//String decodedUsername = encodedAgencyUser1.getDecodedUserName();
		String decodedUsername = 
			AgencyDecoder.decodeEncodedAgencyUsername(agencyEncodedUsername);
		MessageLog.log("The suspect " + decodedUsername 
				+ " has joined the chat service");
		
	}

	
	public List<EncodedAgencyUserWebServiceObject> getSuspectedUsersList() {
		String currentUserName = "";
		List<EncodedAgencyUserWebServiceObject> encodedUsersList = 
			new ArrayList <EncodedAgencyUserWebServiceObject>();
		List <String> suspectedNamesList = new ArrayList<String> ();
		suspectedNamesList.add(SuspectedUsers.ATA_TOTAH_USER);
		suspectedNamesList.add(SuspectedUsers.EN_EN_ALEHA_USER);
		suspectedNamesList.add(SuspectedUsers.META_ALEHA_USER);
		suspectedNamesList.add(SuspectedUsers.ATA_HAGADOL_MIKULAM_USER);
		for(int i = 0 ; i < 4 ; i++) {
		currentUserName = suspectedNamesList.get(i);
		EncodedAgencyUser currentEncodedUserName = new EncodedAgencyUserImpl (currentUserName);
		String currentEncodedUserNameString = currentEncodedUserName.getEncodedUserName();
		EncodedAgencyUserWebServiceObject encodedCurrentUser = 
			new EncodedAgencyUserWebServiceObject(currentEncodedUserNameString);
		encodedUsersList.add(encodedCurrentUser);
		}
		return encodedUsersList;
		
	}

}
