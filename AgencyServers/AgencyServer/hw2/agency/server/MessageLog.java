package hw2.agency.server;
import java.io.*;

/**
 * a class that is responsible for prints
 *
 */
public class MessageLog {

  private static PrintStream _out = System.out;

  public static void log(String message) {
	    _out.println("--------------------------------------------------");
	   _out.println(" [" + new java.util.Date() + "]:");
	    _out.println(message);
	    _out.println("--------------------------------------------------");
	    _out.flush();
	  }

}
