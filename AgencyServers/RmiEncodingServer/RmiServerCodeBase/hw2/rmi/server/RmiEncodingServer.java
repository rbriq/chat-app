package hw2.rmi.server;


import java.io.File;
import java.net.InetAddress;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * The main method prepares the RMI server to accept calls from clients
 * @author NOONA
 *
 */


public class RmiEncodingServer
{
	public static void main (String args [])
	{
		
		/*String codeBasePath =  "file:/C:/Users/NOONA/Desktop/hw22/AgencyServers/RmiEncodingServer/RmiServerClasses/";
		String policyPath = "C:\\Users\\NOONA\\Desktop\\hw22\\"
			+ "AgencyServers\\permissions.policy".replace("\\" , File.separator);
		System.setProperty("java.rmi.server.codebase",codeBasePath);
		System.setProperty("java.security.policy", policyPath);*/
		if (System.getSecurityManager() == null)
		{
			System.setSecurityManager(new RMISecurityManager());
		}

		try
		{
			EncodeInterface encodeInterface = new EncoderImpl();
			Registry registry = LocateRegistry.getRegistry();
			registry.rebind("RmiEncodingServer", encodeInterface);
			MessageLog.log("RmiEncodingServer is running on host " +
					InetAddress.getLocalHost().getHostName() + " with" +
					" address: " + InetAddress.getLocalHost().getHostAddress());
		}
		catch(RemoteException e)
		{
			System.out.println("RmiEncodingServer failed: " + e);
			e.printStackTrace();
		}
		catch (Exception e)
		{
			System.out.println("RmiEncodingServer failed: " + e);
			e.printStackTrace();
		}
	}


}