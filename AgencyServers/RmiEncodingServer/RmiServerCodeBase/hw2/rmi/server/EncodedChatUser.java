package hw2.rmi.server;

//This interface specifies the methods for an EncodedChatUser
//The encoded chat user writes the user name in the "B language" ("sfat habet")
public interface EncodedChatUser
{
	public String getEncodedUserName();
	
	public void setEncodedUserName(String encodedUserName);
	
	public String getDecodedUserName();
}
