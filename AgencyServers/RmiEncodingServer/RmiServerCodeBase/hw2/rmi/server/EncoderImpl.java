package hw2.rmi.server;


import hw2.rmi.server.EncodedAgencyUser;
import hw2.rmi.server.EncodedAgencyUserImpl;
import hw2.rmi.server.EncodedChatUser;
import hw2.rmi.server.EncodedChatUserImpl;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * This class implements the remote methods declared in 
 * EncodeInterface.
 * @author NOONA
 *
 */
public class EncoderImpl extends UnicastRemoteObject implements EncodeInterface{

	
	private static final long serialVersionUID = 2L;

	public EncoderImpl() throws RemoteException{}

	@Override
	public EncodedChatUser decodeEncodedAgencyUsernameForChatSystem(
			String encodedAgencyUsername) throws RemoteException {
		MessageLog.log("the method decodeEncodedAgencyUsernameForChatSystem() has been" +
				"invoked by a remote machine with the encoded agency username:"
				+ encodedAgencyUsername);
		return  new EncodedChatUserImpl(AgencyDecoder.decodeEncodedAgencyUsername(encodedAgencyUsername));
		//return  new EncodedChatUserImpl(encodedAgencyUsername);
	}

	@Override
	public EncodedAgencyUser encodeChatUserForAgency(
			EncodedChatUser encodedChatUser) throws RemoteException {
		MessageLog.log("the method encodeChatUserForAgency() has been" +
				"invoked by a remote machine with the enocded chat username:"
				+ encodedChatUser.getEncodedUserName());
		return  new EncodedAgencyUserImpl(encodedChatUser.getDecodedUserName());
	}
	
}
