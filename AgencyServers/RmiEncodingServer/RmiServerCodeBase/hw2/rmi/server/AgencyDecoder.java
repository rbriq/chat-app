package hw2.rmi.server;

//This class knows how to decode a given encoded by Agency standard user name
public class AgencyDecoder
{
	public static String decodeEncodedAgencyUsername(String encodedAgencyUsername)
	{
		return decodeUserName(encodedAgencyUsername);
	}
	
	private static String decodeUserName(String encodedAgencyUsername)
	{
		String decodedUserName = "";
		int i, len = encodedAgencyUsername.length();
	    StringBuffer dest = new StringBuffer(len);

	    for (i = (len - 1); i >= 0; i--)
	    {
	    	dest.append(encodedAgencyUsername.charAt(i));
	    }
	    decodedUserName = dest.toString();
	    return decodedUserName;
	}
}
