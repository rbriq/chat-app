package hw2.rmi.server;
import java.io.*;

public class MessageLog {

  private static PrintStream _out = System.out;
  
  public static void log(String message) {
	    _out.println("---------------------------");
	   // _out.print(obj.getClass().getName());
	    _out.println(" [" + new java.util.Date() + "]:");
	    _out.println(message);
	    _out.println("---------------------------");
	    _out.flush();
	  }

}
