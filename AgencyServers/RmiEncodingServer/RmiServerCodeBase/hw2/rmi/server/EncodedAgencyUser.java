package hw2.rmi.server;

//This interface specifies the methods for an EncodeAgencyUser
//The encoded agency user writes the user name in a reversed order
public interface EncodedAgencyUser
{
	public String getEncodedUserName();
	
	public void setEncodedUserName(String encodedUserName);
	
	public String getDecodedUserName();
}
