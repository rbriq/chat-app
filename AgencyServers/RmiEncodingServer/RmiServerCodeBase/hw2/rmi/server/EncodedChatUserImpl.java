package hw2.rmi.server;

import java.io.Serializable;


//The encoded chat user writes the user name in the "B language" ("sfat habet")
public class EncodedChatUserImpl implements Serializable, EncodedChatUser
{
	private static final long serialVersionUID = 1L;
	private String encodedUserName = "";
	
	public EncodedChatUserImpl(String userName)
	{
		encodeUserName(userName);
	}

	private void encodeUserName(String userName)
	{
		for(int i=0; i<userName.length(); i++)
		{
			char currChar = userName.charAt(i);
			encodedUserName = encodedUserName.concat(String.valueOf(currChar)).concat("b");
		}
	}
	
	private String decodeUserName()
	{
		String decodedUserName = "";
		boolean include = true;
		for(int i=0; i<encodedUserName.length(); i++)
		{
			if(include)
			{
				char currChar = encodedUserName.charAt(i);
				decodedUserName = decodedUserName.concat(String.valueOf(currChar));
				include = false;
			}
			else
			{
				include = true;
			}
		}
		return decodedUserName;
	}
	
	public String getEncodedUserName()
	{
		return encodedUserName;
	}
	
	public void setEncodedUserName(String encodedUserName)
	{
		this.encodedUserName = encodedUserName;
	}
	
	public String getDecodedUserName()
	{
		return decodeUserName();
	}
	
}
