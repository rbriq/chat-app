package hw2.chat.client;

import java.net.Socket;
import java.net.UnknownHostException;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.InputStreamReader;

import java.lang.Thread;

/**
 * @description This abstract class is the base class for all our chat entities classes.
 * 				All classes that extend this class may connect to servers and join chats.
 * 				This class can connect to servers, send and receive messages, and check the connection status.
 * @super ChatBaseObject
 */
public abstract class ChatEntity extends ChatBaseObject {

	/**
	 * The entity socket. used for connecting to other entities.
	 * */
	private Socket socket;
	/**
	 * The entity socket output stream.
	 * */
	private PrintStream out;
	/**
	 * The entity socket input stream.
	 * */
	private BufferedReader in;
	/**
	 * The entity message acceptor thread.
	 * */
	private Thread msgAcceptorThread;
	/**
	 * The entity message acceptor.
	 * */
	private ChatMessageAcceptor msgAcceptor;
	/**
	 * the entity message sender.
	 * */
	private ChatMessageSender msgSender;
		
	/**
	 * @effects handles incoming messages (called from the message acceptor).
	 * */
	public abstract void acceptMessage(ChatMessage msg);

	/**
	 * @effects handles outgoing messages (called from the message sender).
	 * */
	public abstract void sendMessage(ChatMessage msg);
	
	/**
	 * @effects abstract constructor for ChatEntity with a specific name.
	 * @return new ChatEntity.
	 * @param name - String that names the ChatEntity.
	 */
	public ChatEntity(String name) {
		super(name);
		this.socket = null;
		this.out = null;
		this.in = null;
		this.msgAcceptor = null;
		this.msgAcceptorThread = null;
		this.msgSender = null;
	}	

	
	/**
	 * @effects abstract constructor for ChatEntity with a specific name and socket.
	 * @return new ChatEntity.
	 * @param name - String that names the ChatEntity.
	 * @param socket - an open socket the entity will use.
	 */
	public ChatEntity(String name,Socket socket) {
		super(name);
		this.socket = socket;
		reassignIO();
		this.msgAcceptor = null;
		this.msgAcceptorThread = null;
		this.msgSender = null;
	}
	
	/**
	 * @effects Print a string to the standard output synchronously (Thread safe).
	 * @param s - The message string to print to the default system standard output.
	 * */
	public synchronized static void report(String s) {
		System.out.println(s);
	}

	/**
	 * @effects abstract constructor for ChatEntity with a specific name and socket.
	 * @return new ChatEntity.
	 * @param name - String that names the ChatEntity.
	 * @param socket - an open socket the entity will use.
	 * @param msgSender - chat message sender to use for sending.
	 */
	public ChatEntity(String name,Socket socket, ChatMessageSender msgSender) {
		super(name);
		this.socket = socket;
		reassignIO();
		this.msgAcceptor = null;
		this.msgAcceptorThread = null;
		this.msgSender = msgSender;
	}
	
	/**
	 * @effects Sets the entity message sender.
	 * @param msgSender - A message sender to use when sending out messages.
	 * */
	public void setMessageSender(ChatMessageSender msgSender) {
		this.msgSender = msgSender;
	}
	
	/**
	 * @effects - creates a new ChatMessage with <i>this</i> as the source of the message.
	 * @param msg - The message string content
	 * */
	public ChatMessage createMessage(String msg) {
		return new ChatMessage(this,msg);
	}
	
	/**
	 * @effects Queue a new message on the entity ChatMessageSender with <i>this</i> as the source of the message. if no message sender is assigned nothing is done.
	 * @param msg - The message string to queue
	 * */
	public synchronized void queueMessageForSending(String msg) {
		if(msgSender == null) return;
		msgSender.addMessage(createMessage(msg));
	}
	
	/**
	 * @effects Queue a new message on the entity ChatMessageSender.
	 * @param msg - The ChatMessage to queue.
	 * */
	public synchronized void queueMessageForSending(ChatMessage msg) {
		if(msgSender == null) return;
		msgSender.addMessage(msg);
	}
	
	/**
	 * @return The entity assigned message sender.
	 * */
	public ChatMessageSender getMessageSender() {
		return msgSender;
	}
		
	/**
	 * @effects Create a new ChatMessageAcceptor and Starts a new thread for it. This acceptor handles messages that this entity accepts.
	 * */
	public void startMessageAccepting() {
		msgAcceptor = new ChatMessageAcceptor(this, 0);
		msgAcceptorThread = new Thread(msgAcceptor);
		msgAcceptorThread.start();
	}
	
	/**
	 * @effects Stop the entity message acceptor and the thread it is running on. after calling this method the message acceptor is set to null.
	 * */
	public synchronized void stopMessageAccepting() {
		if(msgAcceptor == null) return;
		msgAcceptor.stopAccepting();
		msgAcceptorThread.interrupt();
		msgAcceptor = null;
		msgAcceptorThread = null;
	}

	/**
	 * @return The entity socket output stream. null if the socket is closed or never opened or and I/O error happened.
	 * */
	public synchronized PrintStream getOutput() {
/*		if (socket == null || socket.isClosed()) return null;
		PrintStream out = null;
		try {
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			out = null;
			printError(name + ": Could not establish socket output stream. closing connection.");
			closeConnection();
		}
		return out;*/
		return out;
	}

	/**
	 * @return The entity socket input stream. null if the socket is closed or never opened or and I/O error happened.
	 * */
	public BufferedReader getInput() {
/*		if (socket == null || socket.isClosed()) return null;
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			in = null;
			printError(name + ": Could not establish socket input stream. closing connection.");
			closeConnection();
		}
		return in;*/
		return in;
	}
		
	/**
	 * @effects try to establish a connection to an address on a specific port.
	 *          if the connection fails for any reason, the socket is set to null and so the inputs and outputs from it.
	 * @requires address != null
	 * @return true if the connection succeeded, false otherwise.
	 * @param address - String containing the address to connect to.
	 * @param port - Integer number for the port to connect to on the given address.
	 */
	public boolean connectTo(String address, Integer port) {
		try {
			closeConnection();
			socket = new Socket(address,port);
			reassignIO();
		} catch (UnknownHostException e) {
			printError("Cannot connect to: " + address + " on port: " + port);
			closeConnection();
		} catch (IOException e) {
			printError("I/O Error while connecting to: " + address + " port: " + port);
			closeConnection();
		}
		return (socket != null);
	}
	
	/**
	 * @effects try to close the current connection of the entity.
	 *          at the end of this method, the Entity socket is set to null and so are the IO from/to it. 
	 */
	public void closeConnection() {
		if (socket != null && socket.isConnected()) {
			try {
				stopMessageAccepting();
				socket.close();
			} catch (IOException e) {
				printError("Unable to close socket.");
			}			
		}
		socket = null;
		in = null;
		out = null;
	}
	
	/**
	 * @effects test to see if this entity is connected to an address.
	 * @return true if the connection open, false otherwise.
	 */
	public synchronized boolean isConnected() {
		if (socket != null && socket.isConnected()) {			
			byte []msg = new byte[1];
			msg[0] = '\n';
			try {
				socket.getOutputStream().write(msg);
			} catch (IOException e) {
				closeConnection();
				return false;
			}
			return true;
		}
		return false;
	}
	
	/**
	 * @effects try to establish the output and input streams from the entities socket.
	 *          on failure the connection is closed and the IO and socket are set to null.
	 */
	private void reassignIO() {
		try {
			out = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			printError("Unable to open socket output.");
			closeConnection();
			return;
		}

		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			printError("Unable to open socket input.");
			closeConnection();
		}
	}
	
	/**
	 * @effects On garbage collection of this entity, the connection is closed and message accepting is stopped.
	 * @throws Throwable if an error accrues. 
	 * */
	@Override
	protected void finalize() throws Throwable {
	    try {
	    	stopMessageAccepting();
	        closeConnection(); 
	        System.out.println(name + ": Connection closed.");
	    } finally {
	        super.finalize();
	    }
	}

}
