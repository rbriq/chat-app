package hw2.chat.client;

import java.util.LinkedList;
import java.lang.Runnable;
import java.lang.InterruptedException;

/**
 * @description Handles message accepting for a ChatEntity.
 * @super ChatBaseObject
 * @implements Runnable
 * */
public class ChatMessageSender extends ChatBaseObject implements Runnable  {
	
	/**
	 * The messages that needs to be handled container.
	 * */
	private LinkedList<ChatMessage> messageQueue;
	/**
	 * The chat entities container that will receive the messages.
	 * */
	private ChatEntityHolder entities;
	/**
	 * The id for this sender.
	 * */
	private Integer id;
	/**
	 * The run stop flag. '1' = stop, '0' = keep running.
	 * */
	private Integer stop;
	
	/**
	 * @effects Create a new ChatMessageSender that is bound to an entity holder.
	 * @param id - The id of this sender.
	 * @param entities - that entity holder that will receive the messages.
	 * */
	public ChatMessageSender(Integer id, ChatEntityHolder entities) {
		super(entities.name + "_MessageSender_" + id);
		this.id = id;
		this.entities = entities;
		messageQueue = new LinkedList<ChatMessage>();
		stop = 0;
	}
	
	/**
	 * @return The sender id (integer).
	 * */
	public Integer getID() {
		return id;
	}
		
	/**
	 * @effects Add a new message to the message container. this message will be sent to the entities in <i>entities</i>.
	 * @param msg - The message to add.
	 * */
	public synchronized void addMessage(ChatMessage msg) {
		messageQueue.addLast(msg);
	}
			
	/**
	 * @effects Remove the first message from the messages queue.
	 * @return The message that was removed.
	 * */
	private synchronized ChatMessage removeMessage() {
		return messageQueue.removeFirst();
	}	
	
	/**
	 * @effects Sets the stop flag so the running thread will stop the sender operation.
	 * */
	public synchronized void stopSending() {
		stop = 1;
	}
		
	/**
	 * @effects Checks every few milliseconds if there are any new messages to send. if there are new messages they are sent. 
	 * */
	public void run() {
		while(stop == 0) {
			
				while(messageQueue.size() == 0) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						return;
					}
				}								
				while(messageQueue.size() != 0) {
					entities.sendMessage(removeMessage());
				}
		}
	}
}

