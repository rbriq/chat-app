package hw2.chat.client;
/**
 * @description This class is the base class for all our chat classes. it support printing of error messages for the
 *              instances of this class. also, it override the hashCode() and equals() methods.
 */
public class ChatBaseObject {
	
	protected String name;
	
	/**
	 * @effects creates a new ChatBaseObject with a specific name.
	 * @return a new ChatBaseObject.
	 * @param name: String that names the object.
	 */
	public ChatBaseObject(String name) {
		this.name = name;
	}
	
	/**
	 * @effects print error message to the system standard error with the prefix of the name of the object.
	 * @return void.
	 * @param msg: the message to print.
	 */
	protected void printError(String msg) {
		System.err.println(name + ": " + msg);
	}
	
	/**
	 * @return int representing the hashCode (this is the object name hash code).
	 */
	public int hashCode() {
		return this.name.hashCode();
	}
	
	/**
	 * @effects check for object equality based on the object name.
	 * @return true id the names match, false otherwise
	 * @param o: the object to compare to.
	 */
	public boolean equals(Object o) {
		if(o instanceof ChatBaseObject) {
			ChatBaseObject tmp = (ChatBaseObject)o;
			return (this.name == tmp.name);
		}
		return false;
	}
	
	/**
	 * @return String containing the name of the object.
	 */
	public String getName() {
		return name;
	}

}

