package hw2.chat.client;

import java.lang.Thread;
import java.lang.Runnable;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * @description Handles message accepting for a ChatEntity.
 * @super ChatBaseObject
 * @implements Runnable
 * */
public class ChatMessageAcceptor extends ChatBaseObject implements Runnable {
	/**
	 * The ID for this acceptor.
	 * */
	private Integer id;
	/**
	 * The ChatEntity it accepts messages for.
	 * */
	private ChatEntity entity;
	/**
	 * The run stop flag. '1' = stop, '0' = keep running.
	 * */
	private Integer stop;

	/**
	 * @effects Create a new ChatMessageAcceptor for an entity woth a specific id.
	 * @param entity - The ChatEnity to accepts messages for.
	 * @param id - The id of this message acceptor (integer).
	 * */
	public ChatMessageAcceptor(ChatEntity entity, Integer id) {
		super(entity.name + "_MessageAcceptor_" + id);
		this.entity = entity;
		this.id = id;
		stop = 0;
	}
	
	/**
	 * @effects retrieve the acceptor id.
	 * @return The id for this acceptor (integer).
	 * */
	public Integer getID() {
		return id;
	}
	
	/**
	 * @effects Sets the stop flag so the running thread will stop the acceptor operation.
	 * */
	public void stopAccepting() {
		stop = 1;
	}
	
	/**
	 * @effects Try to read a String line from the entity socket input stream.
	 * 			When a string is read the entities acceptMessage method is invoked with the new message.
	 * 			On error the function terminates.
	 * */
	public void run() {
		BufferedReader in = entity.getInput();
		if(in == null) {
			printError("Could not establish input stream.");
			return;
		}
		String s="";
		while(s != null && stop == 0) {
				try {
					if(entity.getInput().ready()) {
						s = in.readLine();
						if( s == null ) throw new IOException("null string accepted.");
						entity.acceptMessage(new ChatMessage(entity,s));
					} else {
						Thread.sleep(500);
					}
				} catch (InterruptedException e) {
					s=null;
				} catch (IOException e) {
					printError("Error while reading input.\n\tStopping message accepting.");
					s = null;
				}
		}
	}


}
