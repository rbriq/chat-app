package hw2.chat.client;

import java.lang.Runnable;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.Thread;
import java.lang.NumberFormatException;

/**
 * @description This class is the chat client implementation.
 * 				It is used when connecting to a ChatServer.
 * 				the connection is done by executing:
 * 				java ChatClient <server_address> <server_port>
 * @super ChatEntity
 * @implements Runnable
 */
public class ChatClient extends ChatEntity implements Runnable {

	/**
	 * The client stop flag. '1' = stop. '0' = keep running.
	 * */
	private Integer stop;
	/**
	 * The System standard input stream.
	 * */
	private BufferedReader in;
	/**
	 * The thread that runs the client.
	 * */
	private Thread t;
	
	/**
	 * @effects Creates a new ChatClient. if a connection to the server could not be made, the client terminate the program.
	 * @return A new ChatClient.
	 * @param name - The name for the client.
	 * @param server_addr - The server address to connect to (string).
	 * @param serverPort - The server connection listening port (integer).
	 * */
	public ChatClient(String name,String server_addr,Integer serverPort) {
		super(name);
		stop = 0;
		t = null;
		in = new BufferedReader(new InputStreamReader(System.in));
		connectTo(server_addr, serverPort);
		if (this.getOutput() == null) {
			stopAll();
			System.exit(-1);
		}
	}
	
	/**
	 * @effects Sets the client running thread.
	 * */
	public void setThread(Thread t) {
		this.t = t;
	}

	/**
	 * @effects Handles a message accepted by the message acceptor. 
	 * 			if the message content is ChatMessage.MSG_GOODBYE or ChatMessage.MSG_FULL the client is stopped and the program exits.
	 * @param msg - the message to handle. 
	 * */
	@Override
	public synchronized void acceptMessage(ChatMessage msg) {
		if(msg.getMessage().length() == 0) return;
		ChatEntity.report(msg.getMessage());
		if (msg.getMessage().equals(ChatMessage.MSG_GOODBYE) || msg.getMessage().equals(ChatMessage.MSG_FULL)) {
			stopAll();
			System.exit(1);
		} 
	}

	/**
	 * @effects send a message over the client socket output stream. 
	 * @param msg - the message to send. 
	 * */
	@Override
	public synchronized void sendMessage(ChatMessage msg) {
		getOutput().println(msg.getMessage());
	}
	
	/**
	 * @effects Stops client message accepting, interrupt the client running thread and closes the client connection.
	 * */
	public synchronized void stopAll() {
		stopMessageAccepting();
		stop = 1;
		if (t != null) t.interrupt();
		closeConnection();
	}
	
	/**
	 * @effects This is the running method that is called when starting the client thread. 
	 * 			it handles user message sending and checks if the connection is open.
	 * 			if the connection is closed the client stops running.
	 * */
	@Override
	public void run() {
		String s = "";
		try {
			while(stop == 0 && s != null) {
				s = in.readLine();
				if(s == null) throw new IOException("Input stream error.");
				sendMessage(createMessage(s));
				if(!isConnected()) {
					ChatEntity.report("Server connection lost. disconnecting.");
					stopAll();
					return;
				}
			}
		} catch (IOException e) {
			printError("I/O Error. Terminating connection.");
			stopAll();
			return;
		}
		
	}
	
	/**
	 * @effects Starts a new ChatClient and connects him to a ChatServer. on error, the program terminates.
	 * @param args - The program execution arguments.
	 * @param args[0] - the server address (string).
	 * @param args[1] - the server port (integer).
	 * */
	public static void main(String args[]) {
		if(args.length != 2) {
			System.err.println("Wrong number of arguments.");
			System.exit(-1);
		}
		ChatClient client = null;
		try {
			client = new ChatClient("ChatClient",args[0],Integer.valueOf(args[1]));
		} catch (NumberFormatException e) {
			ChatEntity.report("Could not convert argument 2 to integer: " + args[1]);
			System.exit(-1);
		}
		
		client.startMessageAccepting();		
		Thread t = new Thread(client);
		client.setThread(t);
		t.start();
	}
}

