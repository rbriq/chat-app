package hw2.chat.backend.main;
import java.io.*;

public class MessageLog {

  private static PrintStream _out = System.out;

  
  public static void log(String message) {
	    _out.println("---------------------------");
	    //_out.println(" [" + new java.util.Date() + "]:");
	    _out.println(message);
	    _out.println("---------------------------");
	    _out.flush();
	  }

}
