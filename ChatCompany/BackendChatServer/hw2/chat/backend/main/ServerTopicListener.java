package hw2.chat.backend.main;
import java.util.Properties;

import javax.jms.*;
import javax.naming.*;

/**
 * this class
 * @author NOONA
 *
 */
public class ServerTopicListener {

	private String topicName = "topic1"; //default
	Context jndiContext = null;
	TopicConnectionFactory topicConnectionFactory = null;
	TopicConnection topicConnection = null;
	TopicSession topicSession = null;
	Topic topic = null;
	TopicSubscriber topicSubscriber = null;
	TextListener topicListener = null;
	
	/**
	 * empty constructor
	 */
	public ServerTopicListener(){}
	
	/**
	 * 
	 */
	public ServerTopicListener(String topicName)
	{
		this.topicName = topicName;
	}
	
	public void intialiseListener()
	{
		// TODO comment
	/*	Properties properties = new Properties();
		properties.put("java.naming.factory.initial","org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		properties.put("java.naming.provider.url","tcp://localhost:61616");
		properties.put("java.naming.security.principal","system");
		properties.put("java.naming.security.credentials","manager");
				
		properties.put("connectionFactoryNames","TopicConnectionFactory");
		properties.put("topic.topic1","jms.topic1");*/

		try
		{
			//jndiContext = new InitialContext(properties);
			
			//TODO comment line above, uncomment below
			
			jndiContext = new InitialContext();
		}
		catch (NamingException e)
		{
			System.err.println("Could not create JNDI API " + "context: "
					+ e.toString());
			System.err.println("Backend will not be able to listen to  " + "messages "
					+ "arriving from the central chat server");
			System.err.println("Could not create JNDI API " + "context: "
					+ e.toString());
			//e.printStackTrace();
			//System.exit(1);
		}

		/*
		 * Look up connection factory and topic. If either does not exist, exit.
		 */
		try
		{
			topicConnectionFactory = (TopicConnectionFactory) jndiContext.lookup("TopicConnectionFactory");
			topic = (Topic) jndiContext.lookup(topicName);
		}
		catch (NamingException e)
		{
			System.out.println("JNDI API lookup failed: " + e.toString());
			System.err.println("Backend will not be able to listen to  " + "messages "
					+ "arriving from the central chat server");
			//e.printStackTrace();
			//System.exit(1);
		}
		/*
		 * Create connection. Create session from connection; false means
		 * session is not transacted. Create subscriber. Register message
		 * listener (TextListener). Receive text messages from topic. When all
		 * messages have been received, enter Q to quit. Close connection.
		 */
		try
		{
			topicConnection = topicConnectionFactory.createTopicConnection();
			topicSession = topicConnection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
			topicSubscriber = topicSession.createSubscriber(topic);
			topicListener = new TextListener();
			topicSubscriber.setMessageListener(topicListener); //non durable: receive only messages that are published while they are active.
			
		}
		catch (JMSException e)
		{
			System.out.println("Exception occurred: " + e.toString());
		}
	}
	
	public void listen()
	{
		try 
		{
			topicConnection.start();
			//topicListener.onMessage(message);
			
		} catch (JMSException e) 
		{		
			System.out.println("Exception occurred: " + e.toString());
		}
		catch (Exception e) 
		{		
			System.err.println("Connecting to topic failed:");
			System.err.println("Exception occurred: " + e.toString());
			//exit(-1);
		}
	
	}


	public void closeConnections()
	{
		if (topicConnection != null)
		{
			try
			{
				topicConnection.close();
			}
			catch (JMSException e)
			{
				e.printStackTrace();
			}
		}
	}
}
