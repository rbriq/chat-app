
package hw2.chat.backend.main.generatedfromserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for encodedAgencyUserWebServiceObject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="encodedAgencyUserWebServiceObject">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="encodedUserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "encodedAgencyUserWebServiceObject", propOrder = {
    "encodedUserName"
})
public class EncodedAgencyUserWebServiceObject {

    protected String encodedUserName;

    /**
     * Gets the value of the encodedUserName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncodedUserName() {
        return encodedUserName;
    }

    /**
     * Sets the value of the encodedUserName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncodedUserName(String value) {
        this.encodedUserName = value;
    }

}
