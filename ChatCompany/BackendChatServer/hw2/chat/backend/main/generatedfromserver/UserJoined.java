
package hw2.chat.backend.main.generatedfromserver;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userJoined complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userJoined">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://server.agency.hw2/}encodedAgencyUserWebServiceObject" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userJoined", propOrder = {
    "arg0"
})
public class UserJoined {

    protected EncodedAgencyUserWebServiceObject arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link EncodedAgencyUserWebServiceObject }
     *     
     */
    public EncodedAgencyUserWebServiceObject getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link EncodedAgencyUserWebServiceObject }
     *     
     */
    public void setArg0(EncodedAgencyUserWebServiceObject value) {
        this.arg0 = value;
    }

}
