
package hw2.chat.backend.main.generatedfromserver;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the hw2.chat.backend.main.generatedfromserver package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSuspectedUsersList_QNAME = new QName("http://server.agency.hw2/", "getSuspectedUsersList");
    private final static QName _GetSuspectedUsersListResponse_QNAME = new QName("http://server.agency.hw2/", "getSuspectedUsersListResponse");
    private final static QName _UserJoinedResponse_QNAME = new QName("http://server.agency.hw2/", "userJoinedResponse");
    private final static QName _UserJoined_QNAME = new QName("http://server.agency.hw2/", "userJoined");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: hw2.chat.backend.main.generatedfromserver
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserJoined }
     * 
     */
    public UserJoined createUserJoined() {
        return new UserJoined();
    }

    /**
     * Create an instance of {@link EncodedAgencyUserWebServiceObject }
     * 
     */
    public EncodedAgencyUserWebServiceObject createEncodedAgencyUserWebServiceObject() {
        return new EncodedAgencyUserWebServiceObject();
    }

    /**
     * Create an instance of {@link GetSuspectedUsersList }
     * 
     */
    public GetSuspectedUsersList createGetSuspectedUsersList() {
        return new GetSuspectedUsersList();
    }

    /**
     * Create an instance of {@link GetSuspectedUsersListResponse }
     * 
     */
    public GetSuspectedUsersListResponse createGetSuspectedUsersListResponse() {
        return new GetSuspectedUsersListResponse();
    }

    /**
     * Create an instance of {@link UserJoinedResponse }
     * 
     */
    public UserJoinedResponse createUserJoinedResponse() {
        return new UserJoinedResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSuspectedUsersList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.agency.hw2/", name = "getSuspectedUsersList")
    public JAXBElement<GetSuspectedUsersList> createGetSuspectedUsersList(GetSuspectedUsersList value) {
        return new JAXBElement<GetSuspectedUsersList>(_GetSuspectedUsersList_QNAME, GetSuspectedUsersList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSuspectedUsersListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.agency.hw2/", name = "getSuspectedUsersListResponse")
    public JAXBElement<GetSuspectedUsersListResponse> createGetSuspectedUsersListResponse(GetSuspectedUsersListResponse value) {
        return new JAXBElement<GetSuspectedUsersListResponse>(_GetSuspectedUsersListResponse_QNAME, GetSuspectedUsersListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserJoinedResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.agency.hw2/", name = "userJoinedResponse")
    public JAXBElement<UserJoinedResponse> createUserJoinedResponse(UserJoinedResponse value) {
        return new JAXBElement<UserJoinedResponse>(_UserJoinedResponse_QNAME, UserJoinedResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserJoined }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.agency.hw2/", name = "userJoined")
    public JAXBElement<UserJoined> createUserJoined(UserJoined value) {
        return new JAXBElement<UserJoined>(_UserJoined_QNAME, UserJoined.class, null, value);
    }

}
