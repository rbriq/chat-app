package hw2.chat.backend.main;

/**
 * The TextListener class implements the MessageListener 
 * interface by defining an onMessage method that displays 
 * the contents of a TextMessage.
 *
 * This class acts as the listener for the SimpleTopicSubscriber
 * class.
 */

import hw2.chat.backend.main.generatedfromserver.AgencyServiceServer;
import hw2.chat.backend.main.generatedfromserver.EncodedAgencyUserWebServiceObject;

import javax.jms.*;


public class TextListener implements MessageListener
{
	private AgencyServiceServer agencyServer;

	public TextListener()
	{
		agencyServer = BackendChatServer.getAgencyServer();
	}
	/**
	 * Casts the message to a TextMessage, checks if user who joined now is a suspect by checking the list of 
     * suspect users. if confirmed, inform the intel agency using the method
	 * user joined in agency's server
	 * @param message
	 *            the incoming message
	 */
	public void onMessage(Message message)
	{
		TextMessage msg = null;
		String messageText = "";
		try
		{
			if (message instanceof TextMessage)
			{
				msg = (TextMessage) message;
				messageText =  msg.getText();
				//System.out.println("Reading message: " + messageText);
				handleUsername(messageText);
			}
			else
			{
				System.out.println("Message of wrong type: "
						+ message.getClass().getName());
			}
		}
		catch (JMSException e)
		{
			System.out.println("JMSException in onMessage(): " + e.toString());
		}
		catch (Throwable t)
		{
			System.out.println("Exception in onMessage():" + t.getMessage());
		}
	}
	
	public void handleUsername(String username)
	{
		MessageLog.log("Backend Chat Server: Received the username " 
				+ username  + " from the central chat server,"
				+ " checking if they are a suspect..");
		if (BackendChatServer.isUserSuspect(username))
		{
			System.out.println("User " + username +" is found suspect,"
					+ " reporting their name to the agency");
			EncodedAgencyUserWebServiceObject encodedAgencyUserWebServiceObject = 
				BackendChatServer.getEncodedAgencyUserWebServiceObject(username); 
			//encodedAgencyUserWebServiceObject = objectFactory.createEncodedAgencyUserWebServiceObject();
			//encodedAgencyUserWebServiceObject.setEncodedUserName(messageText);
			if(encodedAgencyUserWebServiceObject != null)
				try
			{
				agencyServer.userJoined(encodedAgencyUserWebServiceObject);
			}
			catch(Throwable t)
			{
				System.out.println("Exception occured while trying to access " +
						"the method provided by the agency web service provider: " +
						"can't report the suspect user " +
						username + " to the agency\n" + t.getMessage());
			}
		}
		else
			System.out.println("User " + username +" is not found suspect");
	}
}