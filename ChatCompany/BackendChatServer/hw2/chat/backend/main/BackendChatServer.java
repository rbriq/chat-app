package hw2.chat.backend.main;

import hw2.chat.backend.main.generatedfromserver.AgencyServiceServer;
import hw2.chat.backend.main.generatedfromserver.AgencyServiceServerImplService;
import hw2.chat.backend.main.generatedfromserver.EncodedAgencyUserWebServiceObject;
import hw2.chat.backend.main.generatedfromserver.ObjectFactory;
import hw2.rmi.server.EncodeInterface;
import hw2.rmi.server.EncodedAgencyUser;
import hw2.rmi.server.EncodedChatUser;
import hw2.rmi.server.EncodedChatUserImpl;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * This class is responsible for communication with the intelligence agency through
 * its webservice and RMI server
 * 
 */
public class BackendChatServer {
	private ServerTopicListener serverTopicListener;
	private static HashSet<String> suspectedUsersSet;
	private static EncodeInterface encodeInterface;
	private static AgencyServiceServer agencyServer;
	

	public BackendChatServer() {
	}

	/**
	 * initialise the hash set of suspected users.
	 * @param rmiPortNumber
	 * @param rmiServerIpAdress
	 */
	private void initialise(String rmiServerIpAdress, int rmiPortNumber) {

		serverTopicListener = new ServerTopicListener();
		suspectedUsersSet = new HashSet<String>();
		try 
		{
		AgencyServiceServerImplService agencyService = new AgencyServiceServerImplService();
		agencyServer = agencyService
				.getAgencyServiceServerImplPort();
	
		List<EncodedAgencyUserWebServiceObject> suspectedUsersList = agencyServer
				.getSuspectedUsersList();
		Iterator<EncodedAgencyUserWebServiceObject> listIter = suspectedUsersList
				.iterator();

		EncodedAgencyUserWebServiceObject currUserWebServiceObject;
			Registry registry = LocateRegistry.getRegistry(rmiServerIpAdress, rmiPortNumber);
			encodeInterface = (EncodeInterface)registry.lookup("RmiEncodingServer");
			MessageLog.log("The following is a list of suspect user names " +
			"as retrieved from the agency server:");
			int i = 1;
			while (listIter.hasNext()) {
		
				currUserWebServiceObject = listIter.next();
				String decodedName = "";
				
					EncodedChatUser ecu = encodeInterface
							.decodeEncodedAgencyUsernameForChatSystem(currUserWebServiceObject
									.getEncodedUserName());
					decodedName = ecu.getDecodedUserName();
					suspectedUsersSet.add(decodedName);
					System.out.println("decoded username " + i + ": " + decodedName);
					++i;
			}
			MessageLog.log("done printing the list of suspect users");
		}
		 catch (RemoteException re) 
		 {
				System.err.println("Could not connect to the agency servers"
			 			+" to retrieve the list of suspect users or decode their names" 
			 			+", Backend server will not "
			 			+"be able to catch suspect users:");
			 
				System.out.println(re);
				//re.printStackTrace();
				//System.exit(-1);
		 }
		catch (Exception e) 
		{
			System.err.println("A connection failure occured while trying to "
		 			+" to retrieve the list of suspect users or decode their names:" 
		 			+", Backend will not "
		 			+"be able to catch suspect users");
		 
			System.out.println("EncodeInterface exception: " + e);
			//e.printStackTrace();
		}
		
	}

	/**
	 * Initialize the topic listener and start listening for messages from the
	 * publisher in the main chat server.
	 * 
	 * @param rmiPortNumber
	 * @param rmiServerIpAdress
	 */
	public void start(String rmiServerIpAdress, int rmiPortNumber) {
		MessageLog.log("Starting the Backend Chat Server");
		initialise(rmiServerIpAdress, rmiPortNumber);
		serverTopicListener.intialiseListener();
		serverTopicListener.listen();
		MessageLog.log("Backend Chat Server is running...");
	}

	/**
	 * Check if the chat user is a suspect
	 * 
	 * @param username
	 *            the username to be checked
	 * @return true if username is a suspect, false otherwise
	 */
	public static boolean isUserSuspect(String username) {
		
		if (suspectedUsersSet.contains(username))
			return true;
		return false;
	}
	
	/**
	 * 
	 * @return AgencyServiceServer: 
	 * 			the agency service server which is used to
	 * 			invoke the methods provided by the web
	 * 			service provider
	 */
	public static AgencyServiceServer getAgencyServer ()
	{
		return agencyServer;
	}

	/**
	 * 
	 * @param username
	 * @return an EncodedAgencyUserWebServiceObject instance that matches the
	 *         username
	 */
	public static EncodedAgencyUserWebServiceObject getEncodedAgencyUserWebServiceObject(
			String username) 
	{
		try 
		{
			EncodedChatUser encodedChatUser = new EncodedChatUserImpl(username);
			EncodedAgencyUser encodedAgencyUser =
				 encodeInterface.encodeChatUserForAgency(encodedChatUser);
			 String agencyEncodedUserName = encodedAgencyUser.getEncodedUserName();
				//EncodedAgencyUserWebServiceObject encodedAgencyUserWebServiceObject = 
					//new EncodedAgencyUserWebServiceObject();
				
				ObjectFactory objectFactory = new ObjectFactory();
				EncodedAgencyUserWebServiceObject encodedAgencyUserWebServiceObject = 
					objectFactory.createEncodedAgencyUserWebServiceObject();
				encodedAgencyUserWebServiceObject.setEncodedUserName(agencyEncodedUserName);
				return encodedAgencyUserWebServiceObject;
		} 
		catch (RemoteException e) 
		{
			System.out.println("invoking remote method " +
					"encodeChatUserForAgency() failed" + e.getMessage());
			System.out.println("failed reporting suspect username " + username 
					+ " to the agency");
			//e.printStackTrace();
		}
		
		return null;
	}

	public static void main(String[] args) {
		if (args.length < 2) {
			// TODO uncomment 2 lines below
			 System.err.println("Rmi Server Address or Rmi Server Port not specified");
			 return;
		}
		// TODO uncomment 2 lines below
		String rmiServerIpAdress = args[0];
		int rmiPortNumber = Integer.parseInt(args[1]);

		BackendChatServer backendChatServer = new BackendChatServer();
		// TODO uncomment and comment
		 backendChatServer.start(rmiServerIpAdress, rmiPortNumber);
		//backendChatServer.start("127.0.0.1", 1099);
	}
}
