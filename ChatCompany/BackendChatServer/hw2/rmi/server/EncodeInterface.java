package hw2.rmi.server;


import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * this is the RMI interface that declares methods
 * that can remotely invoked by an RMI client that 
 * connects to the RMI server that implements this
 * interface, this class is distributed to the 
 * client as well.
 * @author NOONA
 *
 */

public interface EncodeInterface extends Remote{
	
	/**
	 * 
	 * @param encodedChatUser: the user to be encoded according to 
	 * 				the agency standards
	 * @return an encoded user according to the agency standards
	 * @throws RemoteException
	 */
	public EncodedAgencyUser encodeChatUserForAgency(EncodedChatUser encodedChatUser) throws RemoteException;
	
	/**
	 * 
	 * @param encodedAgencyUsername: the username string encoded 
	 * 			according to the agency standards
	 * @return encoded user according to the chat company standards
	 * @throws RemoteException
	 */
	public EncodedChatUser decodeEncodedAgencyUsernameForChatSystem(String encodedAgencyUsername) throws RemoteException;

}
