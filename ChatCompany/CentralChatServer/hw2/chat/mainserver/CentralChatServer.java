package hw2.chat.mainserver;

import java.lang.Thread;
import java.lang.NumberFormatException;
/**
 * @author Dan Shelly: 040951337
 * @description This class is the chat client implementation.
 * 				It is used when connecting to a ChatServer.
 * 				the connection is done by executing:
 * 				java ChatClient <server_address> <server_port>
 * @super ChatEntity
 */
public class CentralChatServer extends ChatEntity {
	/**
	 * The message sending thread.
	 * */
	private Thread msgSenderThread;
	
	/**
	 * The maximal number of client our server should handle.
	 * */
	public static final int MAX_CLIENTS = 5;
	
	/**
	 * Our Company name
	 * */
	public static final String COMPANY_NAME = "ChitChat";
	
	/** 
	 * The time to wait between connection checking.
	 * */
	public static final Long CHECK_INTERVAL = 3000l;
	
	/**
	 * The server clients container
	 * */
	private ChatEntityHolder clients;
	
	/**
	 * Handles connection policies.
	 * */
	private ChatEntityHandler entHandler;
	
	/**
	 * Our server connection acceptor, all connection are accepted though here.
	 * */
	private ChatConnectionListener cnnListener;
	/**The thread that runs the connection listener.*/
	private Thread cnnListenerThread;
	
	/**
	 * Tests our connections every <i>CHECK_INTERVAL</i> milliseconds.
	 * */
	private ChatEntityConnectionTester ecnnTester;
	
	
	/**
	 * @effects Create a new ChatServer and starts its different handling threads.
	 * @param port - The port to use for listening to new connections (integer).
	 * @param name - The server name (string).
	 * */
	public CentralChatServer(Integer port, String name) {
		
		super(name);
		clients = new ChatEntityHolder(name + "_EntityHolder",MAX_CLIENTS,this);
		
		ChatMessageSender msgSender = new ChatMessageSender(0, clients);
		msgSenderThread = new Thread(msgSender);
		msgSenderThread.start();
		setMessageSender(msgSender);
		
		entHandler = new ChatEntityHandler(clients,this);
		entHandler.start();
		
		cnnListener = new ChatConnectionListener(name, port, 0, entHandler);
		cnnListenerThread = new Thread(cnnListener);
		cnnListenerThread.start();
		
		ecnnTester = new ChatEntityConnectionTester(clients,CHECK_INTERVAL);
		ecnnTester.start();
		
		
	}
		
	/**
	 * @effects Stops all server activities and closing the server connection if there was one. all server clients are removed.
	 * */
	private void closeAll() {
		cnnListener.stopListening();
		cnnListenerThread.interrupt();
		clients.closeAll();
		ecnnTester.interrupt();
		entHandler.stopHandling();
		entHandler.interrupt();
		closeConnection();
		msgSenderThread.interrupt();
	}
	/**
	 * @effects When the server is garbage collected, it stops all its activities.
	 * */
	@Override
	protected void finalize() throws Throwable {
	    try {
	        closeAll();
	    } finally {
	        super.finalize();
	    }
	}


	/**
	 * @effects Handles message accepting (if the server is connected to another server). the message is queued for sending.
	 * @param msg - The message that was received
	 * */
	@Override
	public synchronized void acceptMessage(ChatMessage msg) {
		queueMessageForSending(msg);
	}


	/**
	 * @effects Handles message sending to all connected clients.
	 * @param msg - The message to send to all clients.
	 * */
	@Override
	public synchronized void sendMessage(ChatMessage msg) {
		clients.sendMessage(msg);	
	}
	
	/**
	 * @param args - arguments passed to our server.
	 * @param args[0] - The server accepting port (0 for some free port).
	 */
	public static void main(String[] args) {
		if(args.length > 1) {
			System.err.println("Wrong number of arguments.");
			System.exit(-1);
		}
		Integer port = 0;
		try {
			if(args.length == 1) port = Integer.valueOf(args[0]);
			
		} catch(NumberFormatException e) {
			ChatEntity.report("Could not convert argumet to port number: " + args[0]);
			System.exit(-1);
		}
		@SuppressWarnings("unused")
		CentralChatServer server = new CentralChatServer(port,"ChitChatServer");
	}
}
