package hw2.chat.mainserver;

import java.util.HashMap;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author Dan Shelly: 040951337
 * @description This class is responsible for holding all chat entities in our application. the access to the container is synchronized.
 * 				This class also reports its activities while adding and removing entities.
 * @super ChatBaseObject
 */
public class ChatEntityHolder extends ChatBaseObject {

	/**
	 * The possible addition errors.
	 * */
	public enum AddErrors {FULL, EXISTS, NONE }
	/**
	 * The ChatEntity container.
	 * */
	private HashMap<String,ChatEntity> entities;
	/**
	 * The maximal entities this container will hold.
	 * */
	private Integer max_entities;
	/**
	 * The parent entity to use for sending messages. 
	 * */
	private ChatEntity parent;
	
	private ServerTopicPublisher serverTopicPublisher;
	
	/**
	 * @effects creates a new empty <i>ChatEntityHolder</i> object, with a specific name.
	 * @return a new empty <i>ChatEntityHolder</i> object, with a specific name.
	 * @param name - String that names the object.
	 * @param max_entities - Integer representing the maximal number of entities to hold in the container.
	 * @param parent - a parent entity to send messages from.
	 */
	public ChatEntityHolder(String name,Integer max_entities, ChatEntity parent) {
		super(name);
		this.max_entities = max_entities; 
		this.parent = parent;
		entities = new HashMap<String,ChatEntity>();
		serverTopicPublisher = new ServerTopicPublisher();
        serverTopicPublisher.intialisePublisher();
	}
	
	/**
	 * @effects queue a message for sending through the parent entity.
	 * @param msg - String to send from the parent entity.
	 */
	private void queueMessage(String msg) {
		if (parent != null) parent.queueMessageForSending(msg);
	}
		
	/**
	 * @effects adds a new ChatEntity to the container only if the ChatEntity name does not exist in the map and max_entities not yet reached. this method is synchronized.
	 * @return AddErrors.NONE if the addition was successful, AddError.FULL if max_entities reached, AddError.EXIST is the entity name already exist.
	 * @param ce - the ChatEntity to add.
	 */
	public synchronized AddErrors addEntity(ChatEntity ce) {
		if( entities.size() == max_entities ) return AddErrors.FULL;
		if( entities.containsKey(ce.name) ) return AddErrors.EXISTS;
		entities.put(ce.name,ce);
		ChatEntity.report(ce.name + " joined the chat.");
		serverTopicPublisher.publishMessage(ce.name);
		return AddErrors.NONE;
	}
	
	/**
	 * @effects remove a ChatEntity from the container (by name). this method is synchronized.
	 * @return the ChatEntity that was removed, or null if no such entity exist.
	 * @param name - the name of the entity to remove.
	 * @modifies entities
	 */
	public synchronized ChatEntity removeEntity(String name) {
		ChatEntity rem = entities.remove(name);
		if ( rem != null ) {
			queueMessage(name + " left the chat.");
		}
		return rem;		
	}
	
	/**
	 * @effects remove all ChatEntities from the container. this method is synchronized.
	 * @modifies entities
	 */
	public synchronized void clearAll() {
		entities.clear();
	}
	
	/**
	 * @return true if a ChatEntity with this name exist in the container, false otherwise. this method is synchronized.
	 * @param name the name of the entity to check existence for.
	 */
	public synchronized boolean contains(String name) {
		return entities.containsKey(name);
	}
	
	/**
	 * @return ChatEntity with the name specified, null if no such entity exist in the container. this method is synchronized.
	 * @param name the name of the entity to get.
	 */
	public synchronized ChatEntity getEntity(String name) {
		return entities.get(name);
	}
	
	/**
	 * @return true if the container contain '<i>max_entities</i>', false otherwise.  this method is synchronized.
	 */
	public synchronized boolean isFull() {
		return entities.size() == max_entities;
	}
	
	/**
	 * @return integer representing the number of entities in the container. this method is synchronized.
	 */
	public synchronized int entityCount() {
		return entities.size();
	}
	
	/**
	 * @effects sends a message to all Entities in the container. this method is synchronized.
	 * @param msg - ChatMessage to send to all entities in the container.
	 */
	public synchronized void sendMessage(ChatMessage msg) {
		Collection<ChatEntity> c = entities.values();
		Iterator<ChatEntity> iter = c.iterator();
		while(iter.hasNext()) {
			ChatEntity tmp = (ChatEntity)iter.next();
			if( tmp != msg.getSender()) tmp.sendMessage(msg);
		}
	}
	
	/**
	 * @effects Check all the entities connections. if a connection is not connected the entity is removed from the container. this method is synchronized.
	 * */
	public synchronized void checkConnections() {
		Collection<ChatEntity> c = entities.values();
		Iterator<ChatEntity> iter = c.iterator();
		while(iter.hasNext()) {
			ChatEntity tmp = (ChatEntity)iter.next();
			if(!tmp.isConnected()) {
				tmp.closeConnection();
				iter.remove();
				queueMessage(tmp.name + " left the chat.");
				ChatEntity.report(tmp.name + " left the chat.");
			}
		}		
	}
	
	/**
	 * @effects closes all the entities connection and remove them from the container. this method is synchronized.
	 * */
	public synchronized void closeAll() {
		for (ChatEntity ent : entities.values()) {
			ent.sendMessage(new ChatMessage(null,ChatMessage.MSG_GOODBYE));
			ent.closeConnection();
		}
		entities.clear();
	}
	
	

}
