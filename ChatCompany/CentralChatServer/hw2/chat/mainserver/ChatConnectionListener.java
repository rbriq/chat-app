package hw2.chat.mainserver;
import java.net.ServerSocket;
import java.io.IOException;
import java.lang.Runnable;
import java.net.Socket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 
 * @author Dan Shelly: 040951337
 * @description This class is responsible for opening and receiving new connections on a specified port.
 *              this class should be run on a thread in order to achieve maximal concurrency.
 * @implements Runnable
 * @super ChatBaseObject
 */
public class ChatConnectionListener extends ChatBaseObject implements Runnable {

	/**
	 * The server socket that accepts new connections.
	 * */
	private ServerSocket serverSocket;
	/**
	 * The listener ID.
	 * */
	private Integer id;
	/**
	 * The containing server name.
	 * */
	private String server_name;
	/**
	 * The port the listening is done on.
	 * */
	private Integer port;
	/**
	 * The entity handler to use when accepting new connections.
	 * */
	private ChatEntityHandler entityHandler;
	/**
	 * Flag that stops the running of the listener. '1' = stop, '0' = keep running.
	 * */
	private Integer stop;
	
	/**
	 * @effects bind a port to the server socket. if the socket was bound it is unbound.
	 * @return void
	 */
	private void bindPort() {
		if(serverSocket != null && serverSocket.isBound()) {
			try {
				Integer curr_port = serverSocket.getLocalPort();
				serverSocket.close();
				System.out.println("Server: " + server_name + " unbind port: " + curr_port);
			} catch (IOException e) {
				printError("Error while closing port: " + port);	
			}
		}
		serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			printError("Could not bind port: " + port + "\n\tServer will not accept new clients from this port.");
			serverSocket = null;
		}
		
	}
	
	/**
	 * @effects set a new port for the server to listen on, and binds it.
	 * @return void
	 */
	public void setPort(Integer port) {
		this.port = port;
		bindPort();
	}
	
	/**
	 * @return Integer representing the port the server is bound to.
	 */
	public Integer getPort() {
		return port;
	}
	
	/**
	 * @effects create a new Connection Listener to accept incoming connection requests.
	 * @return a new ChatConnectionListener.
	 * @param server_name  : the server name for which this listener is listening for
	 * 		  port         : the port to start listening on (0 means any free port available)
	 * 	      id           : a unique identifier for this Listener (there might be many listeners for each server)
	 *        entityHandler: a class that handles the new connection request.
	 */
	public ChatConnectionListener(String server_name, Integer port, Integer id, ChatEntityHandler entityHandler) {
		super(server_name + "_ConnectionListener_" + id);
		this.stop = 0;
		this.server_name = server_name;
		this.port  = port;
		this.id = id;
		this.entityHandler = entityHandler;
		this.serverSocket = null;
		bindPort();
		if(port == 0 && serverSocket != null) { //in case no specific port was defined
			this.port = serverSocket.getLocalPort();
		}
	}
	
	/**
	 * @effects print the local host information.
	 * @throws UnknownHostException if the information could not be retrieved successfully.
	 */
	private void printServerInformation() throws UnknownHostException {
		ChatEntity.report("Server: " + server_name + 
		           "\n\tIs open on host: " + InetAddress.getLocalHost().getHostName() +
		           "\n\tHost address: " + InetAddress.getLocalHost().getHostAddress() +
		           "\n\tListening on port: " + port);		
	}
	
	public synchronized void stopListening() {
		stop = 1;
	}
	
	/**
	 * @effects start listening on the bounded port on a separate thread.
	 * @requires server socket that is bounded to a valid port and is not closed.
	 */
	public void run() {
		Socket new_socket = null;
		if(serverSocket == null || !serverSocket.isBound() || serverSocket.isClosed()) {
			printError("ServerSocket error.");
			return;
		}
		try {
			printServerInformation();
		} catch (UnknownHostException e) {
			printError("could not get local host information.\n\tNew connections to the serrver would not be accepted.");
			return;
		}
		while(stop == 0) {			
			try {
				new_socket = serverSocket.accept();
				entityHandler.acceptNewEntity(new_socket);
				new_socket = null;
			} catch (IOException e) {
				printError("Could not accept new socket.");
			}
		}
	}
	
	/**
	 * @return Integer holding the id number of this listener instance.
	 */
	public Integer getID() {
		return this.id;
	}
	
}

