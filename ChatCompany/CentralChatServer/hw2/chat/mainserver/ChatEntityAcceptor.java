package hw2.chat.mainserver;

import hw2.chat.mainserver.CentralChatServer;

import java.lang.Runnable;
import java.lang.Thread;
import java.io.IOException;
import java.net.Socket;
import java.io.PrintStream;

/**
 * @author Dan Shelly: 040951337
 * @description This class enforces server policies on new chat entities uppon connection.
 * @super ChatEntity
 * @implement Runnable
 * */
public class ChatEntityAcceptor extends ChatEntity implements Runnable {
	
	/**
	 * The entity holder to add new entities to.
	 * */
	private ChatEntityHolder entHolder;
	/**
	 * The socket to communicate with the new entity.
	 * */
	private Socket socket;
	/**
	 * The server that we connect the entity to.
	 * */
	private CentralChatServer server;
	/**
	 * The run stop flag. '1' = stop, '0' = keep running.
	 * */
	private Integer stop;
	/**
	 * The flag to start accepting messages from the entity. '1' = stop, '0' = start.
	 * */
	private Integer startAccepting;
	
	/**
	 * @return A new ChatEntituAcceptor object.
	 * @param socket - The socket connection is done from and to the new entity.
	 * @param entHolder - The entity holder to add the new entity to.
	 * @param server - The server that the new entity will be connected to.
	 * */
	public ChatEntityAcceptor(Socket socket,ChatEntityHolder entHolder,CentralChatServer server) {
		super(server.getName() ,socket);
		this.socket = socket;
		this.entHolder = entHolder;
		this.server = server;
		stop = 0;
		startAccepting = 1;
		if(!this.isConnected()) stop = 1;
	}
	
	/**
	 * @effects Set the accepting flag to true. all incoming messages from the socket prior to this call are ignored.
	 * */
	private synchronized void startAcceptingNames() {
		startAccepting = 0;
	}
	
	/**
	 * @effects This is the run method that runs until the accepting policy is fulfilled. 
	 */
	public void run() {
		startMessageAccepting();
		sendMessage(createMessage("Please enter your name: "));
		startAcceptingNames();
		try {
			while(stop == 0) Thread.sleep(500);
			if (!isConnected() ) stopAll();
		} catch (InterruptedException e) {
			stopAll();
		}		
	}
	
	private void stopAll() {
		stopMessageAccepting();
		stop = 1;		
	}

	/**
	 * @effects Handles incoming messages from the new entity.
	 * 			The incoming messages are ignored until startAccepting is set to 0.
	 * 			Incoming messages are regarded as names the new entity is choosing for itself. 
	 */
	@Override
	public synchronized void acceptMessage(ChatMessage msg) {
		if(startAccepting == 1 || msg.getMessage().length() == 0) return;
		ChatServerSideEntity ssEnt = null;
		try {
			if(msg == null) throw new IOException("Error Reading Line.");
			if ( entHolder.contains(msg.getMessage()) ) {
				sendMessage(createMessage("Name: " + msg.getMessage() + " already exist, please choose another name: "));
			} else {
				ChatEntityHolder.AddErrors res;
				ssEnt = new ChatServerSideEntity(msg.getMessage(),socket,server);
				res = entHolder.addEntity(ssEnt);
				switch(res) {
					case FULL: //the slot was taken
						sendMessage(new ChatMessage(null,ChatMessage.MSG_FULL));
						ssEnt = null;
						stopAll();
						break;
					case EXISTS: //another user beat him to it
						sendMessage(createMessage("Name: " + msg.getMessage() + " already exist, please choose another name: "));
						ssEnt = null;
						stopAll();
						break;
					case NONE: //Successful connection to the server
						server.queueMessageForSending("******" + ssEnt.getName() + " has just joined the chat room! ******");
						sendMessage(createMessage("Hi " + msg.getMessage() + " and welcome to " + CentralChatServer.COMPANY_NAME + " chatting service. We hope you enjoy your stay, to quit simply type *quit*"));
						ssEnt.startMessageAccepting();
						stopAll();
						break;
				}
			}
		} catch (IOException e) {
			printError("Error while reading line from input.");
			ssEnt = null;
			stopAll();
		}		
	}

	/**
	 * @effects Handles sending of messages to the new entity.
	 */
	@Override
	public synchronized void sendMessage(ChatMessage msg) {
		PrintStream out = this.getOutput();
		if(out == null) {
			stopAll();
			return;
		}
		out.println(msg.compose());
	}
}
