package hw2.chat.mainserver;

/**
 * @author Dan Shelly: 040951337
 * @description The chat message object. contains the message content and the message sender.
 * */
public class ChatMessage {

	public static final String MSG_FULL = "Sorry, our server is currently full. Please try again later.";
	public static final String MSG_QUIT = "*quit*";
	public static final String MSG_GOODBYE = "Goodbye";
	
	/**
	 * The sender of the message.
	 * */
	private ChatEntity from;
	/**
	 * The message content.
	 * */
	private String msg;
	
	/**
	 * @effects Utility function. used to compose a message from a String and a ChatBaseObject
	 * @param cbo - The Chat object to use his name.
	 * @param msg - The message content.
	 * */
	public static String composeMessage(ChatBaseObject cbo, String msg) {
		return cbo.getName() + ": " + msg; 
	}
	
	/**
	 * @return A new ChatMessage.
	 * @param from - The sender of the message.
	 * @param msg - The message content.
	 * */
	public ChatMessage(ChatEntity from, String msg) {
		this.from = from;
		this.msg = msg;
	}
	
	/**
	 * @return The message sender.
	 * */
	public ChatEntity getSender() {
		return from;
	}
	
	/**
	 * @return The message content (string).
	 * */
	public String getMessage() {
		return msg;
	}
	
	/**
	 * @return A String that represent the message content and the sender.
	 * */
	public String compose() {
		if(from != null)
			return from.getName() + ": " + msg;
		else
			return msg;

	}	
}


