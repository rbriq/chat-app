package hw2.chat.mainserver;

import hw2.chat.mainserver.CentralChatServer;

import java.net.Socket;
import java.util.LinkedList;
import java.lang.Thread;

/**
 * @author Dan Shelly: 040951337
 * @description This class is used to handle new connections to the ChatServer.
 * @super Thread
 */
public class ChatEntityHandler extends Thread {
	
	/**
	 * The new sockets that needs to be handled.
	 * */
	private LinkedList<Socket> new_sockets;
	/**
	 * The entity container that we want to add new entities to.
	 * */
	private ChatEntityHolder entHolder;
	/**
	 * The ChatServer that the socket connects to.
	 * */
	private CentralChatServer server;
	/**
	 * Flag that stops the running of the entity handler. '1' = stop, '0' = keep running.
	 * */
	private Integer stop;
	
	/**
	 * @effects Create a new ChatEntityHandler.
	 * @return A new ChatEntityHandler
	 * @param entHolder - The entity holder to use for new connections.
	 * @param server - The server this handler is running on.
	 * */
	public ChatEntityHandler(ChatEntityHolder entHolder, CentralChatServer server) {
		this.entHolder = entHolder;
		this.server = server;
		stop = 0;
		new_sockets = new LinkedList<Socket>();
	}
	/**
	 * @effects print an error message to the standard error.
	 * @param msg - The message to print on the standard error stream.
	 * */
	private void printError(String msg) {
		System.err.println(server.getName() + "_EntityHandler: " + msg);
	}

	/**
	 * @effects adds a new socket to the <i>new_socket</i> container. this method is synchronized.
	 * */
	public synchronized void acceptNewEntity(Socket s) {
		new_sockets.addLast(s);
	}
	
	/**
	 * @effects remove a new socket from the <i>new_socket</i> container. this method is synchronized.
	 * */
	public synchronized Socket removeSocket() {
		return new_sockets.removeFirst();
	}
	
	/**
	 * @effects sets the stop flag to '1' to indicate the thread to stop running.
	 * */
	public synchronized void stopHandling() {
		stop = 1;
	}
	
	/**
	 * @effects This is the run method that is called when this handler is started.
	 * */
	@Override
	public void run() {
		try {
			while(stop == 0) {
				while(new_sockets.size() == 0) Thread.sleep(1000);
				while(new_sockets.size() != 0) {
					Thread t = new Thread(new ChatEntityAcceptor(removeSocket(),entHolder,server));
					t.start();
				}
			}
		} catch (InterruptedException e) {
			printError("Interrupted while sleeping.");
		}
	}
}
