package hw2.chat.mainserver;
import java.util.Properties;

import javax.jms.*;
import javax.naming.*;


	/**
	 * The SimpleTopicPublisher class publishes messages to a topic. 
	 * Run this program in conjunction with SimpleTopicSubscriber.  
	 * Specify a topic name on the command line when you run the
	 * program. 
	 */
	public class ServerTopicPublisher
	{
		private String topicName = "topic1";
		private Context jndiContext = null;
		private TopicConnectionFactory topicConnectionFactory = null;
		private TopicConnection topicConnection = null;
		private TopicSession topicSession = null;
		private Topic topic = null;
		private TopicPublisher topicPublisher = null;
		private TextMessage message = null;
		
		/**
		 * empty constructor
		 */
		public ServerTopicPublisher(){}
		
		/**
		 * 
		 * @param topicName
		 */
		public ServerTopicPublisher(String topicName)
		{
			this.topicName = topicName;
		}
		
		/**
		 * initialise the topic publisher variables
		 */
		public void intialisePublisher()
		{
			/*Properties properties = new Properties();
			properties.put("java.naming.factory.initial","org.apache.activemq.jndi.ActiveMQInitialContextFactory");
			properties.put("java.naming.provider.url","tcp://localhost:61616");
			properties.put("java.naming.security.principal","system");
			properties.put("java.naming.security.credentials","manager");
					
			properties.put("connectionFactoryNames","TopicConnectionFactory");
			properties.put("topic.topic1","jms.topic1");}*/

			try
			{
				//jndiContext = new InitialContext(properties);
				jndiContext = new InitialContext();
			}
			catch (NamingException e)
			{
				System.out.println("Could not create JNDI API " + "context: " + e.toString());
				e.printStackTrace();
				//System.exit(1);
			}
			try
			{
				topicConnectionFactory = (TopicConnectionFactory) jndiContext.lookup("TopicConnectionFactory");
				topic = (Topic) jndiContext.lookup(topicName); //topicName needs to be defined in the jndi.properties file!
			}
			catch (NamingException e)
			{
				System.out.println("JNDI API lookup failed: " + e.toString());
				e.printStackTrace();
				System.exit(1);
			}
			try
			{
				topicConnection = topicConnectionFactory.createTopicConnection(); //an active connection to a JMS Pub/Sub provider
				topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE); //provides methods for creating TopicPublisher, TopicSubscriber
					//transacted - indicates whether the session is transacted
					//acknowledgeMode - indicates whether the consumer or the client will acknowledge any messages it receives; 
				    //					ignored if the session is transacted. Legal values are Session.AUTO_ACKNOWLEDGE, 
					//					Session.CLIENT_ACKNOWLEDGE, and Session.DUPS_OK_ACKNOWLEDGE
				topicPublisher = topicSession.createPublisher(topic);
				message = topicSession.createTextMessage();					
			}
			catch (JMSException e)
			{
				System.out.println("Central chat server could not connect to the"
						+" backend server, central server will not be able to publish" +
								" names of users who join");
				System.out.println("Exception occurred: " + e.toString());
				e.printStackTrace();
			}
		}
		
		/**
		 * publishes a text message
		 * @param messageText: the text message text to be published
		 */
		public void publishMessage(String messageText)
		{
			try 
			{
				message.setText(messageText);
				System.out.println("Publishing message to the backend chat server: " 
						+ message.getText());
				topicPublisher.publish(message);
			} 
			catch (JMSException e) 
			{
				System.out.println("Exception occurred: " + e.toString());
				System.out.println("Could not publish name to the Backend server");
				//e.printStackTrace();
			}
			catch (Exception e) 
			{
				System.out.println("Exception occurred: " + e.toString());
				System.out.println("Could not publish name to the Backend server");
				//e.printStackTrace();
				closeConnections();
			}
			
		}
		
		/**
		 * close connection and clean resources
		 */
			public void closeConnections()
			{	
				if (topicConnection != null)
				{
					try
					{
						topicConnection.close();
					}
					catch (JMSException e)
					{
						System.out.println("Exception occurred: " + e.toString());
						e.printStackTrace();
					}
				}
				if (topicPublisher != null)
				{
					try
					{
						topicConnection.close();
					}
					catch (JMSException e)
					{
						System.out.println("Exception occurred: " + e.toString());
						e.printStackTrace();
					}
				}
			}
	}