package hw2.chat.mainserver;

import hw2.chat.mainserver.CentralChatServer;

import java.net.Socket;
import java.io.PrintStream;

/**
 * @author Dan Shelly: 040951337
 * @description This is the ChatEntity representative the ChatServer saves for communication.
 * @super ChatEntity
 * */
public class ChatServerSideEntity extends ChatEntity {

	/**
	 * The server the entity belongs to.
	 */
	private CentralChatServer server;
	
	/**
	 * @return A new ChatServerSideEntity object.
	 * @param name - The name of the new entity.
	 * @param socket - The socket to communicate with the entity.
	 * @param server - The server the entity is connected to.
	 */
	public ChatServerSideEntity(String name, Socket socket,CentralChatServer server) {
		super(name, socket,server.getMessageSender());		
		this.server = server;
	}
		
	/**
	 * @effects Handles message accepting from the entity socket.
	 * @param msg - The message that was accepted.
	 */
	@Override
	public synchronized void acceptMessage(ChatMessage msg) {
		if(msg.getMessage().length() == 0) return;
		if(msg.getMessage().equals(ChatMessage.MSG_QUIT)) {
			sendMessage(new ChatMessage(null,ChatMessage.MSG_GOODBYE));
			closeConnection();
		} else {
			server.acceptMessage(msg);
		}
	}
	
	/**
	 * @effects Handle message sending to the entity.
	 * @param msg - The message to transfer to the entity.
	 */
	@Override
	public synchronized void sendMessage(ChatMessage msg) {
		PrintStream out;
		out = getOutput();
		if(out != null) out.println(msg.compose());
	}
	
}
