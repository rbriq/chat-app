package hw2.chat.mainserver;

import java.lang.InterruptedException;
import java.lang.Thread;

/**
 * @author Dan Shelly: 040951337
 * @description This class is responsible to check every so-often the connection of each entity in the entity holder.
 * 				It runs in a separate thread.
 * @super java.lang.Thread
 * */
public class ChatEntityConnectionTester extends Thread {

	/**
	 * The entity container to check connections on.
	 */
	private ChatEntityHolder entHolder;
	/**
	 * The time interval between each check (in milliseconds).
	 */
	private Long interval;
	
	/**
	 * @return A new ChatEntityConnectionTester object.
	 * @param entHolder - The entity container to check.
	 * @param interval - The time interval between checks (in milliseconds).
	 */
	public ChatEntityConnectionTester(ChatEntityHolder entHolder,Long interval) {
		this.entHolder = entHolder;
		this.interval = interval;
	}
	
	/**
	 * @effects The run method that is called by the executing thread.
	 * 			The method will check the entities connections and then go to sleep for <i>interval</i> milliseconds.
	 */
	@Override
	public void run() {
		try{
			while(true) {
				entHolder.checkConnections();
				Thread.sleep(interval);
			}
		} catch (InterruptedException e) {
			return;
		}
		
	}

}
